<!DOCTYPE html>
<html>
    <head>
        <title> Form Sign Up </title>
        <meta charset="UTF-8">
    </head>

    <body>
        <!-- Tag Heading-->
        <div>
        <h1> Buat Account baru! </h1>
        <h3> Sign Up Form </h3>
        </div>

        <form action="/welcome" method="POST">
            @csrf
            <label for="first_name"> First Name:</label>
            <br><br>
            <input TYPE="text" name="first_name" >
            <br><br>
            <label for="last_name"> Last Name:</label>
            <br><br>
            <input TYPE="text" name="last_name">
            <br><br>
            <label> Gender: </label>
            <br><br>
            <input type="radio" name="gender" value="0"> Male
            <br>
            <input type="Radio" name="gender" value="1"> Female
            <br><br>
            <label>Nationality:</label>
            <br><br>
            <select>
                <option value="Indonesian">Indonesian</option>
                <option value="Singaporian">Singaporian</option>
                <option value="Malaysian">Malaysian</option>
                <option value="Australian">Australian</option>
            </select>
            <br><br>
            <label>Languange Spoken:</label>
            <br><br>
            <input type="checkbox"> Bahasa Indonesia<br>
            <input type="checkbox"> English<br>
            <input type="checkbox"> Others
            <br><br>
            <label>Bio:</label>
            <br><br>
            <textarea cols="50" rows="7"></textarea><br>
            <input type="submit" value="Sign Up">
            
        
        </form>

    </body>
</html>